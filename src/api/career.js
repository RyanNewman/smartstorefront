import Request from '@/libs/request'

export const careerList = (reqData, token) => {
  return new Request('manage/object/career/list', reqData, token).post()
}

export const careerAdd = (reqData, token) => {
  return new Request('manage/object/career/add', reqData, token).post()
}

export const careerDel = (reqData, token) => {
  return new Request('manage/object/career/del', reqData, token).post()
}

export const careerModify = (reqData, token) => {
  return new Request('manage/object/career/modify', reqData, token).post()
}
