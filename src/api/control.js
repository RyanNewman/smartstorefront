import Request from '@/libs/request'

export const controlList = (reqData, token) => {
  return new Request('manage/object/control/list', reqData, token).post()
}

export const controlRegister = (reqData, token) => {
  return new Request('manage/object/control/register', reqData, token).post()
}

export const controlUpdate = (reqData, token) => {
  return new Request('manage/object/control/update', reqData, token).post()
}

export const controlAdd = (reqData, token) => {
  return new Request('manage/object/control/add', reqData, token).post()
}

export const controlDel = (reqData, token) => {
  return new Request('manage/object/control/del', reqData, token).post()
}

export const controlAccList = (reqData, token) => {
  return new Request('manage/object/control/control-acc-list', reqData, token).post()
}

export const controlStoreAccList = (reqData, token) => {
  return new Request('manage/object/control/store-acc-list', reqData, token).post()
}

export const storeBoxAccList = (reqData, token) => {
  return new Request('manage/object/control/box-acc-list', reqData, token).post()
}
