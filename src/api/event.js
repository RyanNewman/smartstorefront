import Request from '@/libs/request'

export const eventList = (reqData, token) => {
  return new Request('manage/event/list', reqData, token).post()
}
