import Request from '@/libs/request'

export const logList = (reqData, token) => {
  return new Request('log/list', reqData, token).post()
}
