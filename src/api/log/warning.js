import Request from '@/libs/request'

export const warningList = (reqData, token) => {
  return new Request('log/warning', reqData, token).post()
}
