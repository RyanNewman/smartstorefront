import Request from '@/libs/request'

export const adminAdd = (reqData, token) => {
  return new Request('manage/user/add', reqData, token).post()
}

export const adminList = (reqData, token) => {
  return new Request('manage/user/list', reqData, token).post()
}

export const adminDel = (reqData, token) => {
  return new Request('manage/user/del', reqData, token).post()
}

export const adminRoleList = (reqData, token) => {
  return new Request('manage/user/role-info', reqData, token).post()
}

export const adminUpdate = (reqData, token) => {
  return new Request('manage/user/update', reqData, token).post()
}
