import Request from '@/libs/request'

export const permissionList = (reqData, token) => {
  return new Request('permission/permission/list', reqData, token).post()
}

export const allPermissionList = (reqData, token) => {
  return new Request('permission/permission/all-permission-list', reqData, token).post()
}

export const allPublicList = (reqData, token) => {
  return new Request('permission/permission/all-public-list', reqData, token).post()
}

export const allUnitList = (reqData, token) => {
  return new Request('permission/permission/all-unit-list', reqData, token).post()
}

export const permissionUpdate = (reqData, token) => {
  return new Request('permission/permission/update', reqData, token).post()
}

export const permissionAdd = (reqData, token) => {
  return new Request('permission/permission/add', reqData, token).post()
}

export const permissionDel = (reqData, token) => {
  return new Request('permission/permission/del', reqData, token).post()
}
