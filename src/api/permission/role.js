import Request from '@/libs/request'

export const allRoleList = (reqData, token) => {
  return new Request('permission/role/all', reqData, token).post()
}

export const roleList = (reqData, token) => {
  return new Request('permission/role/list', reqData, token).post()
}

export const roleAdd = (reqData, token) => {
  return new Request('permission/role/add', reqData, token).post()
}

export const roleDel = (reqData, token) => {
  return new Request('permission/role/del', reqData, token).post()
}
