import Request from '@/libs/request'

export const statusList = (reqData, token) => {
  return new Request('status/list', reqData, token).post()
}
