import Request from '@/libs/request'

export const unitList = (reqData, token) => {
  return new Request('manage/object/unit/list', reqData, token).post()
}

export const unitAdd = (reqData, token) => {
  return new Request('manage/object/unit/add', reqData, token).post()
}

export const unitDel = (reqData, token) => {
  return new Request('manage/object/unit/del', reqData, token).post()
}

export const unitModify = (reqData, token) => {
  return new Request('manage/object/unit/modify', reqData, token).post()
}
