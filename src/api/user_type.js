import Request from '@/libs/request'

export const userTypeList = (reqData, token) => {
  return new Request('manage/object/usertype/list', reqData, token).post()
}
