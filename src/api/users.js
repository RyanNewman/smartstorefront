import Request from '@/libs/request'

export const userList = (reqData, token) => {
  return new Request('manage/object/user/list', reqData, token).post()
}

export const userAdd = (reqData, token) => {
  return new Request('manage/object/user/add', reqData, token).post()
}

export const userDel = (reqData, token) => {
  return new Request('manage/object/user/del', reqData, token).post()
}

export const userAppoint = (reqData, token) => {
  return new Request('manage/object/user/duty-change', reqData, token).post()
}

export const userModify = (reqData, token) => {
  return new Request('manage/object/user/modify', reqData, token).post()
}
