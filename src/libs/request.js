import axios from 'axios'
import config from '@/config'
const baseUrl = process.env.NODE_ENV === 'development' ? config.baseUrl.dev : config.baseUrl.pro

/*
默认请求类型为json,可手动设置为其它类型
函数最后会返回axios的请求实体的Promise对象
 */
class Request {
  constructor (url, data, token) {
    this.axios = axios // axios实例
    this._encode = false // 编码是否被转换,如果请求发送前还为false将自动将数据转换为json
    this._sendBeforeHook = null // 发送前的hook,当设置了hook函数时发送前会自动执行
    this.option = {
      url: baseUrl + url,
      method: '',
      timeout: 1000,
      maxContentLength: 20000,
      data: data,
      // `responseType` 表示服务器响应的数据类型，可以是 'arraybuffer', 'blob', 'document', 'json', 'text', 'stream'
      responseType: 'json', // 默认的
      headers: {
        'Content-type': 'application/json'
      }
    } // axios的主要配置
    if (typeof token !== 'undefined') {
      this.option.headers['jwt'] = token
    } // 如果实例初始化时传入了token将自动加上token码
    this.setSendBeforeHook(() => {
      if (this._encode === false) {
        this.toJson() // 发送前将数据默认转为json
      }
    }) // 设置发送前的hook函数
  }

  // 将发送数据转换为json编码
  toJson () {
    this.option.data = JSON.stringify(this.option.data)
    this.encode = true
    return this
  }

  // 设置响应头
  setResponseType (str) {
    this.option.responseType = str
    return this
  }

  // 设置http基础认证
  setAuth (username, password) {
    this.option.auth.username = username
    this.option.auth.password = password
    return this
  }

  // 设置请求超时,单位秒
  setTimeOut (second) {
    this.option.timeout = second * 1000
    return this
  }

  // 设置最大响应数据大小
  setMaxContentLength (len) {
    this.option.maxContentLength = len
    return this
  }

  // 设置发送时的mime-type
  setMimeType (mime) {
    this.option.headers['Content-type'] = mime
    return this
  }

  // 设置token
  setToken (token) {
    this.option.headers.jwt = token
    return this
  }

  // 设置请求头
  setHeader (key, value) {
    this.option.headers[key] = value
    return this
  }

  // 设置发送前的hook函数
  setSendBeforeHook (hook) {
    this._sendBeforeHook = hook
    return this
  }

  // 发送前的hook函数,每个发送函数在发送请求前自动调用
  sendBeforeHook () {
    if (this._sendBeforeHook !== null) {
      this._sendBeforeHook()
    }
  }

  // get请求函数
  get () {
    this.sendBeforeHook()
    this.option.method = 'get'
    return this.axios(this.option)
  }

  // post请求函数
  post () {
    this.sendBeforeHook()
    this.option.method = 'post'
    return this.axios(this.option)
  }

  // delete请求函数
  delete () {
    this.sendBeforeHook()
    this.option.method = 'delete'
    return this.axios(this.option)
  }

  // head请求函数
  head () {
    this.sendBeforeHook()
    this.option.method = 'head'
    return this.axios(this.option)
  }

  // options请求函数
  options () {
    this.sendBeforeHook()
    this.option.method = 'options'
    return this.axios(this.option)
  }

  // put请求函数
  put () {
    this.sendBeforeHook()
    this.option.method = 'put'
    return this.axios(this.option)
  }

  // patch请求函数
  patch () {
    this.sendBeforeHook()
    this.option.method = 'patch'
    return this.axios(this.option)
  }
}

export default Request
