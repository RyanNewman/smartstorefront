import Vue from 'vue'
import Vuex from 'vuex'
import user from './module/user'
import app from './module/app'
import event from './module/event'
import unit from './module/unit'
import control from './module/control'
import users from './module/users'
import permission from './module/permission/permission'
import role from './module/permission/role'
import admin from './module/permission/admin'
import career from './module/career'
import logLog from './module/log/log'
import warningLog from './module/log/warning'
import status from './module/status/status'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //
  },
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    user,
    app,
    event,
    unit,
    control,
    users,
    permission,
    role,
    admin,
    career,
    logLog,
    warningLog,
    status
  }
})
