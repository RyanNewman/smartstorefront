/**
 * 单位数据保存
 */
import { careerList, careerAdd, careerDel, careerModify } from '@/api/career'
import { getToken } from '@/libs/util'

export default {
  state: {
    careerList: {
      data: [],
      total: 0
    }
  },
  getters: {
    careerList: state => {
      return state.careerList.data
    },
    careerCount: state => {
      return state.careerList.total
    }
  },
  mutations: {
    setCareerList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.careerList.data = data
      }
      if (typeof total !== 'undefined') {
        state.careerList.total = total
      }
    }
  },
  actions: {
    handleCareerList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        careerList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              console.log(resBody)
              commit('setCareerList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('职位获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    // 添加单位
    handleCareerAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        careerAdd(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('职位添加成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    // 删除单位
    handleCareerDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        careerDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('职位删除成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleCareerModify ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        careerModify(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('职位修改成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleCareerClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setCareerList', {
            data: [],
            total: 0
          })
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
