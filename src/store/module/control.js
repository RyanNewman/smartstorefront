/**
 * 控制柜相关数据保存
 */
import { controlList, controlRegister, controlDel, controlUpdate } from '@/api/control'
import { unitList } from '@/api/unit'
import { getToken } from '@/libs/util'

export default {
  state: {
    // 控制柜列表相关信息
    controlList: {
      list: [],
      count: 0
    },
    // 单位列表相关信息，用于控制柜注册时下拉选择框数据初始化
    unitList: {
      list: [],
      count: 0
    }
  },
  getters: {
    controlList: state => {
      let list = state.controlList.list
      for (let i = 0; i < list.length; i++) {
        list[i].units = (() => {
          let dataArray = []
          let splitArray = []
          let string = list[i].units
          let regex = /,/
          splitArray = string.split(regex)
          let temp = ''
          for (let i = 0; i < splitArray.length; i++) {
            if (i % 2 === 0) {
              temp = splitArray[i]
            } else {
              dataArray.push({
                name: temp,
                id: splitArray[i]
              })
            }
          }
          return dataArray
        })()
      }
      return list
    },
    controlCount: state => {
      return state.controlList.count
    },
    controlUnitList: state => {
      return state.unitList.list
    },
    controlUnitCount: state => {
      return state.unitList.count
    }
  },
  mutations: {
    setControlList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.controlList.list = data
      }
      if (typeof total !== 'undefined') {
        state.controlList.count = total
      }
    },
    setControlUnitList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.unitList.list = data
      }
      if (typeof total !== 'undefined') {
        state.unitList.count = total
      }
    }
  },
  actions: {
    handleControlList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setControlList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('控制柜列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleControlUnitList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setControlUnitList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('单位列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleControlRegister ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlRegister(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message)
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleControlDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message)
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleControlUpdate ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlUpdate(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message)
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleControlClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setControlList', {
            data: [],
            total: 0
          })
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
