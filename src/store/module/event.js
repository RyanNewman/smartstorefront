import { eventList } from '@/api/event'
import { getToken, unixToDate, objectIterator, mapToObject, eventName } from '@/libs/util'

export default {
  state: {
    eventList: {
      item: [],
      total: 0
    }
  },
  getters: {
    eventList: state => {
      let lastData = []
      state.eventList.item.map(list => {
        let eventData = new Map()
        eventData.set('over_time', unixToDate(parseInt(list.create_time) + parseInt(list.duration)))
        objectIterator(list, (k, v) => {
          switch (k) {
            case 'create_time':
              eventData.set(k, unixToDate(v))
              break
            case 'remand_time':
              if (v !== '') {
                eventData.set(k, unixToDate(v))
              } else {
                eventData.set(k, v)
              }
              break
            case 'start_wait_time':
              if (v !== '') {
                eventData.set(k, unixToDate(v))
              } else {
                eventData.set(k, v)
              }
              break
            case 'status':
              eventData.set(k, (() => {
                return eventName(v)
              })())
              break
            default:
              eventData.set(k, v)
          }
        })
        lastData.push(mapToObject(eventData))
      })
      return lastData
    },
    eventCount: state => {
      return state.eventList.total
    }
  },
  mutations: {
    setEventList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.eventList.item = data
      }
      if (typeof total !== 'undefined') {
        state.eventList.total = total
      }
    }
  },
  actions: {
    handleEventList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        eventList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setEventList', { total: res.data.total, data: resBody }) // 提交mutations
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleEventClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setEventList', {
            data: [],
            total: 0
          })
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
