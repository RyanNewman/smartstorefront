/**
 * 日志信息
 */
import { logList } from '@/api/log/log'
import { getToken, unixToDate } from '@/libs/util'

export default {
  state: {
    logList: {
      data: [],
      total: 0
    }
  },
  getters: {
    logList: state => {
      return (() => {
        let data = []
        for (let v of state.logList.data) {
          v.time = unixToDate(v.time)
          data.push(v)
        }
        return data
      })()
    },
    logCount: state => {
      return state.logList.total
    }
  },
  mutations: {
    setLogList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.logList.data = data
      }
      if (typeof total !== 'undefined') {
        state.logList.total = total
      }
    }
  },
  actions: {
    handleLogList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        logList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setLogList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('日志列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    }
  }
}
