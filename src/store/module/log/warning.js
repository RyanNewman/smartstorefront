/**
 * 日志信息
 */
import { warningList } from '@/api/log/warning'
import { getToken, unixToDate } from '@/libs/util'

export default {
  state: {
    warningList: {
      data: [],
      total: 0
    }
  },
  getters: {
    warningList: state => {
      return (() => {
        let data = []
        for (let v of state.warningList.data) {
          v.time = unixToDate(v.time)
          data.push(v)
        }
        return data
      })()
    },
    warningCount: state => {
      return state.warningList.total
    }
  },
  mutations: {
    setWarningList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.warningList.data = data
      }
      if (typeof total !== 'undefined') {
        state.warningList.total = total
      }
    }
  },
  actions: {
    handleWarningList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        warningList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setWarningList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('警告信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    }
  }
}
