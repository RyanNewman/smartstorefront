/**
 * 权限数据保存
 */
import { adminAdd, adminDel, adminList, adminRoleList, adminUpdate } from '@/api/permission/admin'
import { getToken } from '@/libs/util'

export default {
  state: {
    adminList: {
      data: [],
      total: 0
    },
    adminRoleList: []
  },
  getters: {
    adminList: state => {
      return state.adminList.data
    },
    adminCount: state => {
      return state.adminList.total
    },
    adminRoleList: state => {
      return state.adminRoleList
    }
  },
  mutations: {
    setAdminList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.adminList.data = data
      }
      if (typeof total !== 'undefined') {
        state.adminList.total = total
      }
    },
    setAdminRoleList (state, data) {
      state.adminRoleList = data
    }
  },
  actions: {
    handleAdminList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        adminList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAdminList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('管理员列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAdminAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        adminAdd(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('管理员添加成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAdminDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        adminDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('管理员删除成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAdminRoleList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        adminRoleList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAdminRoleList', resBody) // 提交mutations
              resolve('角色信息获取成功')
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAdminUpdate ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        adminUpdate(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('信息更新成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAdminClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setAdminList', {
            data: [],
            total: 0
          })
          commit('setAdminRoleList', [])
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
