/**
 * 权限数据保存
 */
import { allPermissionList, allPublicList, allUnitList, permissionList, permissionUpdate, permissionAdd, permissionDel } from '@/api/permission/permission'
import { getToken } from '@/libs/util'

export default {
  state: {
    allPermissionList: {}, // 所有的权限，用于选择框数据显示
    allPublicList: {},
    allUnitList: {},
    permissionList: {
      data: [],
      total: 0
    } // 用于权限页面表格实时显示
  },
  getters: {
    permissionList: state => {
      return state.permissionList.data
    },
    permissionCount: state => {
      return state.permissionList.total
    },
    allPermissionList: state => {
      return state.allPermissionList
    },
    allPublicList: state => {
      return state.allPublicList
    },
    allUnitList: state => {
      return state.allUnitList
    }
  },
  mutations: {
    setPermissionList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.permissionList.data = data
      }
      if (typeof total !== 'undefined') {
        state.permissionList.total = total
      }
    },
    setAllPermissionList (state, data) {
      state.allPermissionList = data
    },
    setAllPublicList (state, data) {
      state.allPublicList = data
    },
    setAllUnitList (state, data) {
      state.allUnitList = data
    }
  },
  actions: {
    handlePermissionList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        permissionList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setPermissionList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('权限列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAllPermissionList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        allPermissionList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAllPermissionList', resBody) // 提交mutations
              resolve('权限列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAllPublicList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        allPublicList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAllPublicList', resBody) // 提交mutations
              resolve('公共权限列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAllUnitList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        allUnitList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAllUnitList', resBody) // 提交mutations
              resolve('公共权限列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handlePermissionUpdate ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        permissionUpdate(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('权限更新成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handlePermissionAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        permissionAdd(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('权限添加成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handlePermissionDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        permissionDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('权限删除成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handlePermissionClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setPermissionList', {
            data: [],
            total: 0
          })
          commit('setAllPermissionList', {})
          commit('setAllPublicList', {})
          commit('setAllUnitList', {})
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
