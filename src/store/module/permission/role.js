/**
 * 权限数据保存
 */
import { roleList, roleAdd, roleDel, allRoleList } from '@/api/permission/role'
import { getToken } from '@/libs/util'

export default {
  state: {
    roleList: {
      data: [],
      total: 0
    },
    allRoleList: []
  },
  getters: {
    roleList: state => {
      return state.roleList.data
    },
    roleCount: state => {
      return state.roleList.total
    },
    allRoleList: state => {
      return state.allRoleList
    }
  },
  mutations: {
    setRoleList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.roleList.data = data
      }
      if (typeof total !== 'undefined') {
        state.roleList.total = total
      }
    },
    setAllRoleList (state, data) {
      state.allRoleList = data
    }
  },
  actions: {
    handleRoleList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        roleList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setRoleList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('角色列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleAllRoleList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        allRoleList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setAllRoleList', resBody) // 提交mutations
              resolve('角色列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleRoleAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        roleAdd(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('角色添加成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleRoleDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        roleDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('角色删除成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleRoleClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setRoleList', {
            data: [],
            total: 0
          })
          commit('setAllRoleList', [])
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
