/**
 * 状态信息
 */
import { statusList } from '@/api/status/status'
import { getToken, eventName } from '@/libs/util'

const colorFul = (num) => {
  switch (num) {
    case 1:
      return '#2d8cf0'
    case 2:
      return '#10A6FF'
    case 3:
      return '#0C17A6'
    case 4:
      return '#4608A6'
    case 5:
      return '#398DBF'
    default:
      return '#ff9900'
  }
}

export default {
  state: {
    status: {
      info_card_data: [
        { title: '用户数量', icon: 'md-people', count: 0, color: '#2d8cf0' },
        { title: '已使用控制柜', icon: 'md-locate', count: 0, color: '#19be6b' },
        { title: '总控制柜', icon: 'md-add-circle', count: 0, color: '#ff9900' },
        { title: '告警数量', icon: 'md-warning', count: 0, color: '#ed3f14' },
        { title: '事件数量', icon: 'md-list', count: 0, color: '#E46CBB' },
        { title: '单位数量', icon: 'md-map', count: 0, color: '#9A66E4' }
      ],
      pie_data: [
        { 'value': 0, 'name': '空' }
      ],
      bar_data: {
        '周一': 0,
        '周二': 0,
        '周三': 0,
        '周四': 0,
        '周五': 0,
        '周六': 0,
        '周日': 0
      },
      broken_line: [
        {
          name: '默认',
          type: 'line',
          stack: '总量',
          areaStyle: { normal: {
            color: '#2d8cf0'
          } },
          data: [0, 0, 0, 0, 0, 0, 0]
        }
      ]
    }
  },
  getters: {
    infoCardData: state => {
      return state.status.info_card_data
    },
    pieData: state => {
      return state.status.pie_data
    },
    barData: state => {
      return state.status.bar_data
    },
    brokenLine: state => {
      return state.status.broken_line
    }
  },
  mutations: {
    setStatus (state, data) {
      state.status.info_card_data = [
        { title: '用户数量', icon: 'md-people', count: data.info_card_data.user_number, color: '#2d8cf0' },
        { title: '已使用控制柜', icon: 'md-locate', count: data.info_card_data.register_control_number, color: '#19be6b' },
        { title: '总控制柜', icon: 'md-add-circle', count: data.info_card_data.control_number, color: '#ff9900' },
        { title: '告警数量', icon: 'md-warning', count: data.info_card_data.warning_number, color: '#ed3f14' },
        { title: '事件数量', icon: 'md-list', count: data.info_card_data.event_number, color: '#E46CBB' },
        { title: '单位数量', icon: 'md-map', count: data.info_card_data.unit_number, color: '#9A66E4' }
      ]
      if (data.pie_data !== null) {
        state.status.pie_data = (() => {
          let pieData = []
          for (let v of data.pie_data) {
            pieData.push({
              'name': eventName(v.name),
              'value': v.value
            })
          }
          return pieData
        })()
      }
      state.status.bar_data = {
        '周一': data.bar_data.mon,
        '周二': data.bar_data.tue,
        '周三': data.bar_data.wed,
        '周四': data.bar_data.thu,
        '周五': data.bar_data.fri,
        '周六': data.bar_data.sat,
        '周日': data.bar_data.sun
      }
      if (data.broken_line !== null) {
        state.status.broken_line = (() => {
          let datas = []
          let i = 0
          for (let v of data.broken_line) {
            i++
            let item = {
              name: v.name,
              type: 'line',
              stack: '总量',
              areaStyle: { normal: {
                color: colorFul(i)
              } },
              data: v.data
            }
            datas.push(item)
          }
          return datas
        })()
      }
    }
  },
  actions: {
    handleStatus ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        statusList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setStatus', resBody) // 提交mutations
              resolve('状态信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    }
  }
}
