/**
 * 单位数据保存
 */
import { unitList, unitAdd, unitDel, unitModify } from '@/api/unit'
import { getToken } from '@/libs/util'

export default {
  state: {
    unitList: {
      data: [],
      total: 0
    }
  },
  getters: {
    unitList: state => {
      return state.unitList.data
    },
    unitCount: state => {
      return state.unitList.total
    }
  },
  mutations: {
    setUnitList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.unitList.data = data
      }
      if (typeof total !== 'undefined') {
        state.unitList.total = total
      }
    }
  },
  actions: {
    handleUnitList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUnitList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('单位列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    // 添加单位
    handleUnitAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitAdd(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('单位添加成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    // 删除单位
    handleUnitDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitDel(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('单位删除成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUnitModify ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitModify(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve('单位修改成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUnitClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setUnitList', {
            data: [],
            total: 0
          })
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
