/**
 * 控制柜相关数据保存
 */
import { userList, userAdd, userDel, userAppoint, userModify } from '@/api/users'
import { unitList } from '@/api/unit'
import { careerList } from '@/api/career'
import { userTypeList } from '@/api/user_type'
import { controlAccList, controlStoreAccList, storeBoxAccList } from '@/api/control'
import { getToken } from '@/libs/util'

export default {
  state: {
    // 用户列表，用户表格数据展示
    userList: {
      data: [],
      total: 0
    },
    /**
     * 单位列表，用户用户添加时下拉选择框渲染
     * 此列表需要在控制柜被选择时实时更新
     * 并且当用户选择的单位与控制柜不匹配时弹出错误提示
     */
    userUnitList: [],
    // 职位列表，用于用户注册时的职位选择
    userCareerList: [],
    // 用户类型列表，用户用户注册时类型选择
    userTypeList: [],
    // 控制柜列表
    userControlList: [],
    // 存储柜列表，当用户选择控制柜后，存储柜信息实时更新显示
    userStoreList: [],
    // 储物格列表，当用户选定相应控制柜后，自动展示未被绑定的储物格
    userBoxList: [] // 储物格列表
  },
  getters: {
    userList: state => {
      return state.userList.data
    },
    userCount: state => {
      return state.userList.total
    },
    userUnitList: state => {
      return state.userUnitList
    },
    userCareerList: state => {
      return state.userCareerList
    },
    userTypeList: state => {
      return state.userTypeList
    },
    userControlList: state => {
      return state.userControlList
    },
    userStoreList: state => {
      return state.userStoreList
    },
    userBoxList: state => {
      return state.userBoxList
    }
  },
  mutations: {
    setUserList (state, { data, total }) {
      if (typeof data !== 'undefined') {
        state.userList.data = data
      }
      if (typeof total !== 'undefined') {
        state.userList.total = total
      }
    },
    setUserUnitList (state, data) {
      state.userUnitList = data
    },
    setUserCareerList (state, data) {
      state.userCareerList = data
    },
    setUserTypeList (state, data) {
      state.userTypeList = data
    },
    setUserControlList (state, data) {
      state.userControlList = data
    },
    setUserStoreList (state, data) {
      state.userStoreList = data
    },
    setUserBoxList (state, data) {
      state.userBoxList = data
    }
  },
  actions: {
    handleUserList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        userList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserList', { total: res.data.total, data: resBody }) // 提交mutations
              resolve('用户列表获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    // 用户添加
    handleUserAdd ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      } else {
        return new Promise((resolve, reject) => {
          userAdd(reqData, token)
            .then(res => {
              if (res.data.code === 0) {
                resolve('用户添加成功')
              } else {
                reject(res.data.message)
              }
            }).catch(err => {
              reject(err)
            })
        })
      }
    },
    // 用户删除
    handleUserDel ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      } else {
        return new Promise((resolve, reject) => {
          userDel(reqData, token)
            .then(res => {
              if (res.data.code === 0) {
                resolve('用户删除成功')
              } else {
                reject(res.data.message)
              }
            }).catch(err => {
              console.trace(err)
              reject(err)
            })
        })
      }
    },
    handleUserUnitList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        unitList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserUnitList', resBody) // 提交mutations
              resolve('单位信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserCareerList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        careerList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserCareerList', resBody) // 提交mutations
              resolve('职位信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserTypeList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        userTypeList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserTypeList', resBody) // 提交mutations
              resolve('用户类型信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserControlList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlAccList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserControlList', resBody) // 提交mutations
              resolve('用户控制柜信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserStoreList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        controlStoreAccList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserStoreList', resBody) // 提交mutations
              resolve('用户存储柜信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserBoxList ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        storeBoxAccList(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              let resBody = JSON.parse(res.data.data)
              commit('setUserBoxList', resBody) // 提交mutations
              resolve('用户储物格信息获取成功')
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserAppoint ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        userAppoint(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message)
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserModify ({ commit }, reqData) {
      // 获取认证码
      let token = getToken()
      if (token === false) {
        return new Promise((resolve, reject) => {
          reject(new Error('认证码失效'))
        })
      }
      return new Promise((resolve, reject) => {
        userModify(reqData, token)
          .then(res => {
            if (res.data.code === 0) {
              resolve(res.data.message)
            } else {
              reject(res.data.message)
            }
          }).catch(err => {
            reject(err)
          })
      })
    },
    handleUserClean ({ commit }) {
      return new Promise((resolve, reject) => {
        try {
          commit('setUserList', {
            data: [],
            total: 0
          })
          commit('setUserUnitList', [])
          commit('setUserCareerList', [])
          commit('setUserTypeList', [])
          commit('setUserControlList', [])
          commit('setUserStoreList', [])
          commit('setUserBoxList', [])
          resolve()
        } catch (e) {
          reject(e)
        }
      })
    }
  }
}
